# AngularEffects

A simple project demonstrating ngrx effects.
See src/app/store/effects to find the 'todo-effects' class and it's unit tests.

The onUpdateTodo$ effect simply logs to the console whenever the updateTodo store action is dispatched. The corresponding unit test mocks a dispatch event and spies on the console.log method.

The onClearTodos$ effect returns the result of a service call after the clearTodos store action is dispatched. The corresponding unit test asserts the correct value is returned.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
