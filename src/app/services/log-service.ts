import { of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class LogService {
  logEvent(event: string) {
    // dummy api response
    return of(event);
  }
}
