import { Action, createReducer, on } from '@ngrx/store';
import * as TodoActions from '../actions/todo-actions';

export const TodoFeatureKey = 'TodoFeature';

export interface TodoState {
  todo: {
    id: number,
    name: string,
    description: string
  };
}

export const initialState: TodoState = {
  todo: {
    id: 0,
    name: 'first todo',
    description: 'the first thing'
  }
};

export const reducer = createReducer(
  initialState,
  on(TodoActions.updateTodo, (state, action) => ({ ...state, todo: action.todo })),
  on(TodoActions.clearTodos, () => initialState)
);

export function TodoReducer(state: TodoState | undefined, action: Action) {
  return reducer(state, action);
}
