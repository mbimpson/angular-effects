import { createAction, props } from '@ngrx/store';
import { TodoModel } from 'src/app/model/todo/todo';

export const UPDATE_TODO = 'UPDATE_TODO';
export const CLEAR_TODOS = 'CLEAR_TODOS';

export const updateTodo = createAction(UPDATE_TODO, props<{ todo: TodoModel }>());
export const clearTodos = createAction(CLEAR_TODOS);
