import { TestBed } from '@angular/core/testing';
import { TodoEffects } from './todo-effects';
import { StoreModule, Action } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import { LogService } from 'src/app/services/log-service';
import { CLEAR_TODOS, UPDATE_TODO } from '../actions/todo-actions';

describe('TodoEffects', () => {
  let actions$: Observable<Action>;
  let effects: TodoEffects;

  beforeEach((() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({}),
      ],
      declarations: [

      ],
      providers: [
        TodoEffects,
        LogService,
        provideMockActions(() => actions$)
      ],
    }).compileComponents();

    effects = TestBed.get(TodoEffects);
  }));

  it('should log to the console when updateTodo is dispatched', () => {
    const spy = spyOn(console, 'log');
    actions$ = of({
      type: UPDATE_TODO,
      payload: {
        todo: {
          id: 0,
          name: 'test',
          description: 'test description'
        }
      }
    });
    effects.onUpdateTodo$.subscribe(action => {
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should return the effect value when clearTodos is dispatched', () => {
    actions$ = of({
      type: CLEAR_TODOS
    });
    effects.onClearTodos$.subscribe(result => {
      expect(result).toBe('todos reset');
    });
  });
});
