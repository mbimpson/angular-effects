import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap, switchMap, catchError } from 'rxjs/operators';
import { updateTodo, clearTodos } from '../actions/todo-actions';
import { LogService } from 'src/app/services/log-service';
import { of } from 'rxjs';

export class TodoEffects {
  constructor(private actions: Actions, private logService: LogService) {}

  // This effect simply logs to the console from within the effect
  onUpdateTodo$ = createEffect(
    () => this.actions.pipe(
      ofType(updateTodo),
      tap(() => console.log('updateTodo store action dispatched'))
    ),
    { dispatch: false }
  );

  // This effect returns something
  onClearTodos$ = createEffect(
    () => this.actions.pipe(
      ofType(clearTodos),
      switchMap(() => this.logService.logEvent('todos reset')),
      catchError(error => {
        console.log(error);
        return of(error);
      })
    ),
    { dispatch: false }
  );
}
