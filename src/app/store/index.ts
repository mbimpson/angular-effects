import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import * as fromTodo from './reducers/todo-reducer';

export interface State {
  todo: fromTodo.TodoState;
}

export const reducers: ActionReducerMap<State> = {
  todo: fromTodo.reducer
};

export const metaReducers: MetaReducer<State>[] = [];
