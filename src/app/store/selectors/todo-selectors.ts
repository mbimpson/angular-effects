import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as todoReducer from '../reducers/todo-reducer';

export const selectTodoState = createFeatureSelector<todoReducer.TodoState>(
  todoReducer.TodoFeatureKey
);

export const selectTodo = createSelector(
  selectTodoState,
  state => state.todo
);
