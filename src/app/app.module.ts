import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToDoComponent } from './to-do/to-do.component';
import { StoreModule } from '@ngrx/store';
import * as todoReducer from './store/reducers/todo-reducer';
import { reducers, metaReducers } from './store';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { TodoEffects } from './store/effects/todo-effects';
import { EffectsModule } from '@ngrx/effects';
import { LogService } from './services/log-service';

@NgModule({
  declarations: [
    AppComponent,
    ToDoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forFeature(todoReducer.TodoFeatureKey, todoReducer.reducer),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument(),
    EffectsModule.forRoot([TodoEffects])
  ],
  providers: [LogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
