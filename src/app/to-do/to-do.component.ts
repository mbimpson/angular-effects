import { Component, OnInit, OnDestroy } from '@angular/core';
import { TodoState } from '../store/reducers/todo-reducer';
import { select, Store } from '@ngrx/store';
import { selectTodo } from '../store/selectors/todo-selectors';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { TodoModel } from '../model/todo/todo';
import { updateTodo, clearTodos } from '../store/actions/todo-actions';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.less']
})
export class ToDoComponent implements OnInit, OnDestroy {

  constructor(private store: Store<TodoState>) {
    this.todo$ = this.store.pipe(select(selectTodo));
  }

  todo: TodoModel;
  todo$: Observable<any>;
  // todo$: BehaviorSubject<any>;
  todoSubscription: Subscription;

  ngOnInit() {
    this.todoSubscription = this.todo$.pipe(
        map((x: any) => {
          this.todo = x;
        })
      )
      .subscribe();
  }

  ngOnDestroy() {
    if (this.todoSubscription) {
      this.todoSubscription.unsubscribe();
    }
  }

  updateTodo() {
    this.store.dispatch(updateTodo({ todo: {...this.todo, name: 'updated name'} }));
  }

  clearTodos() {
    this.store.dispatch(clearTodos());
  }
}
